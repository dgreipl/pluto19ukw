package haw_landshut.de.pluto19ukw;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.ArrayList;

import haw_landshut.de.pluto19ukw.model.Post;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = "XX MainActivity";

    // Adapter für Listview
    ArrayAdapter<Post> mAdapter;

    // List der empfangenen Posts
    ArrayList<Post> mPostList;

    // Referenz auf die ListView in der Layout Datei
    ListView mListView;

    // Query zur Abfrage der Posts
    Query mPostQuery;

    // Läuft der Listener schon?
    boolean mListenerRunning = false;

    ChildEventListener mCel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG,"onCreate");
        setContentView(R.layout.activity_main);

        // Enable off line
        FirebaseDatabase.getInstance().setPersistenceEnabled( true );

        // Initialisere die Variable mCel;
        mCel = getChildEventListener();

        // Initialisiert mPostList als leere Liste;
        mPostList  = new ArrayList<>();

        // Initialiseren des Adapters;
        mAdapter = new ArrayAdapter<Post>(
                this,
                android.R.layout.simple_list_item_2,
                android.R.id.text1,
                mPostList
        ){
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view =  super.getView(position, convertView, parent);

                TextView text1, text2;

                text1 =  view.findViewById( android.R.id.text1 );
                text2 =  view.findViewById( android.R.id.text2);

                Post post = getItem( getCount() - position  - 1);

                if (post != null) {
                    text1.setText(post.author);
                    text2.setText(post.body);
                }
                return view;

            }
        };

        mListView = findViewById( R.id.mainListViewMessages);
        mListView.setAdapter( mAdapter );

        mPostQuery = FirebaseDatabase.getInstance().getReference()
                .child("Posts/").limitToLast(5);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate( R.menu.menu_main, menu );
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;

        switch ( item.getItemId() ){

            case R.id.mainPost:
                intent = new Intent( getApplication(), PostActivity.class);
                startActivity( intent );
                return true;

            case R.id.mainManageAccount:
                intent = new Intent(getApplication(), ManageAccountActivity.class);
                startActivity(intent);
                return true;

            case R.id.mainCrash:
                Crashlytics.getInstance().crash();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onStart() {
        super.onStart();

        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            // App auf Startzustand zurück
            resetApp();

            Intent intent = new Intent(getApplication(), SignInActivity.class);
            startActivity(intent);
        }
        else {
            if (!mListenerRunning) {
                mPostQuery.addChildEventListener( mCel );
                mListenerRunning = true;
                mAdapter.notifyDataSetChanged();
            }
        }
        Log.d(TAG,"onStart");

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG,"onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG,"onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG,"onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG,"onDestroy");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG,"onReStart");
    }


    private ChildEventListener getChildEventListener(){
        ChildEventListener cel;
        cel = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                Post p = Post.postFromSnapshot( dataSnapshot );
                mPostList.add( p );
                mAdapter.notifyDataSetChanged();

                Log.d(TAG, "Event: onChildAdded. Key des datasnapShot :" + dataSnapshot.getKey());

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Log.d(TAG, "Event: onChildChanged. Key des datasnapShot :" + dataSnapshot.getKey());

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

                String snapshotKey = dataSnapshot.getKey();

                for (int i = 0; i < mPostList.size(); i++){
                    if (mPostList.get(i).firebaseKey.equals( snapshotKey )){
                        mPostList.remove( i );
                        Log.d(TAG, "Removed enty "+ i);
                        break;
                    }
                }

                mAdapter.notifyDataSetChanged();

                Log.d(TAG, "Event: onChildRemoved. Key des datasnapShot :" + dataSnapshot.getKey());

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                Log.d(TAG, "Event: onChildMoved. Key des datasnapShot :" + dataSnapshot.getKey());

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "Event:  onCancelled. Listener was cancelled");
                mListenerRunning = false;
            }
        };

        return cel;

    }

    void resetApp(){

        // Listener stoppen
        if (mListenerRunning){
            mPostQuery.removeEventListener( mCel );
            mListenerRunning = false;
        }

        mPostList.clear();
        mAdapter.notifyDataSetChanged();
    }
}