package haw_landshut.de.pluto19ukw;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;

import java.util.HashMap;
import java.util.Map;


public class PostActivity extends AppCompatActivity
        implements OnClickListener{

    EditText mPostTitle;
    EditText mPostBody;
    Button mPostButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);

        mPostTitle = findViewById( R.id.postTitle);
        mPostBody  = findViewById( R.id.postText);
        mPostButton = findViewById( R.id.postButtonPost);
        mPostButton.setOnClickListener( this );


    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        switch (i) {
            case R.id.postButtonPost:
                doPost();
                return;
            default:
        }
    }

    private void doPost() {
        Map<String,Object> postMap = new HashMap<>();

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        // Zur zur Sicherheit...
        if (user == null){
            return;
        }

        postMap.put("uid", user.getUid() );
        postMap.put("author", user.getEmail() );
        postMap.put("title", mPostTitle.getText().toString() );
        postMap.put("body", mPostBody.getText().toString() );
        postMap.put("timestamp", ServerValue.TIMESTAMP );

        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("Posts/");
        mDatabase.push().setValue( postMap );

        Toast.makeText(getApplicationContext(), "Post geschrieben", Toast.LENGTH_LONG).show();
    }
}