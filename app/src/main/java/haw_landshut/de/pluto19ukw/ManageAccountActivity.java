package haw_landshut.de.pluto19ukw;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class ManageAccountActivity extends AppCompatActivity
        implements View.OnClickListener {

    private final String TAG ="xx ManageAccount";

    TextView mEmail, mAccountState, mTechnicalId;
    EditText mPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_account);


        mEmail = findViewById(R.id.manageAccountEmail);
        mAccountState = findViewById(R.id.manageAccountVerificationState);
        mTechnicalId = findViewById(R.id.manageAccountTechnicalId);
        mPassword = findViewById(R.id.manageAccountPassword);

        findViewById(R.id.manageAccountButtonSignOut).setOnClickListener(this);
        findViewById(R.id.manageAccountButtonSendActivationMail).setOnClickListener(this);
        findViewById(R.id.manageAccountButtonDeleteAccount).setOnClickListener(this);


        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        mEmail.setText("Mail : " + user.getEmail());
        mAccountState.setText("Account verified:" + user.isEmailVerified());
        mTechnicalId.setText("ID : " + user.getUid());
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        switch (i) {
            case R.id.manageAccountButtonDeleteAccount:
                doDeleteAccount();
                return;
            case R.id.manageAccountButtonSignOut:
                doSignOut();
                return;
            case R.id.manageAccountButtonSendActivationMail:
                doSendActivationMail();
                return;
            default:
        }
    }

    private void doSendActivationMail() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user == null) {

            Toast.makeText(getApplication(), "You are not signed in.", Toast.LENGTH_LONG).show();
            return;
        }

        if (user.isEmailVerified()) {

            Toast.makeText(getApplication(), "Account already verified.", Toast.LENGTH_LONG).show();
            return;
        }

        user.sendEmailVerification().addOnCompleteListener(
                this,
                new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(getApplication(), "Mail sent", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getApplication(), "Sending mail failed", Toast.LENGTH_LONG).show();
                            Log.d(TAG, task.getException().getLocalizedMessage());
                        }
                    }
                }
        );
    }

    private void doDeleteAccount() {

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {

            user.delete().addOnCompleteListener(
                    this,
                    new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Toast.makeText(getApplication(), "Account deleted", Toast.LENGTH_LONG).show();
                                finish();
                            } else {
                                Toast.makeText(getApplication(), "Account deletion failed", Toast.LENGTH_LONG).show();
                                Log.d(TAG, task.getException().getLocalizedMessage());
                            }

                        }
                    }
            );
        }
    }

    private void doSignOut() {
        String msg;
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user == null) {
            msg = "Not authenticated. Cannot sign out.";
        } else {
            FirebaseAuth.getInstance().signOut();
            msg = "Your are signed out";
            finish();
        }
        Toast.makeText(getApplication(), msg, Toast.LENGTH_LONG).show();
    }
}
