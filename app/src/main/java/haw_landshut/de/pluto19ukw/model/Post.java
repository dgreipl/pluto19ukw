package haw_landshut.de.pluto19ukw.model;

import com.google.firebase.database.DataSnapshot;

public class Post {
    public String uid;  // von welchem user stammt der post
    public String author; // Name dieses users
    public String title;
    public String body;
    public long timestamp;  // Zeitstempel des Post am Server (wann wurde der Post geschrieben);
    public String firebaseKey;

    public Post() {
    }

    public static Post postFromSnapshot(DataSnapshot dataSnapshot){
        Post p = new Post();

        p.uid = (String) dataSnapshot.child("uid").getValue();
        p.author = (String) dataSnapshot.child("author").getValue();
        p.title = (String) dataSnapshot.child("title").getValue();
        p.body = (String) dataSnapshot.child("body").getValue();
        p.timestamp = (long) dataSnapshot.child("timestamp").getValue();

        p.firebaseKey = dataSnapshot.getKey();

        return p;
    }

    public Post(String uid, String author, String title, String body, long timestamp) {
        this.uid = uid;
        this.author = author;
        this.title = title;
        this.body = body;
        this.timestamp = timestamp;
    }


}
