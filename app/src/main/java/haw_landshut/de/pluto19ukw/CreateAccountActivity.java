package haw_landshut.de.pluto19ukw;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class CreateAccountActivity extends AppCompatActivity  implements View.OnClickListener {

    private final String TAG ="xx CreateAccount";

    private EditText mEditTextEMail;
    private EditText mEditTextPassword1;
    private EditText mEditTextPassword2;
    private Button mButtonCreateAccount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);
        mEditTextEMail = findViewById(R.id.createAccountEMail);
        mEditTextPassword1 = findViewById(R.id.createAccountPassword1);
        mEditTextPassword2 = findViewById(R.id.createAccountPassword2);
        mButtonCreateAccount = findViewById(R.id.createAccountButtonCreateAccount);

        mButtonCreateAccount.setOnClickListener( this );

    }

    @Override
    public void onClick(View v) {
        switch ( v.getId()) {
            case R.id.createAccountButtonCreateAccount:
                doCreateAccount();
                return;
            default:
        }
    }

    private void doCreateAccount(){
        // TODO: Add check of password equality
        // TODO: Add check of password rules
        FirebaseAuth.getInstance().createUserWithEmailAndPassword(
                mEditTextEMail.getText().toString(),
                mEditTextPassword1.getText().toString()).addOnCompleteListener(
                this,
                new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(getApplication(), "Account created", Toast.LENGTH_LONG).show();
                            finish();
                        } else {
                            Toast.makeText(getApplication(), "Account creation failed", Toast.LENGTH_LONG).show();
                            Log.d(TAG, task.getException().getLocalizedMessage());
                        }

                    }
                }

        );
    }
}